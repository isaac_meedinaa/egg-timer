package example.isaac.eggtimer;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    SeekBar skb1;
    TextView tv1;
    Button btn1;

    MediaPlayer mediaPlayer;
    CountDownTimer countDownTimer;

    Boolean counterIsActive = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        skb1 = (SeekBar) findViewById(R.id.skb_one);
        timeSlider();

        tv1 = (TextView) findViewById(R.id.tv_one);
        btn1 = (Button) findViewById(R.id.btn_one);

        mediaPlayer = MediaPlayer.create(this, R.raw.airhorn);
    }

    public void timeSlider() {
        skb1.setMax(600);
        skb1.setProgress(30);
        skb1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                updateTimer(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void countdownTimer(View view) {

        if (counterIsActive) {
            resetTimer();
        } else {
            counterIsActive = true;
            skb1.setEnabled(false);
            btn1.setText("STOP");


            countDownTimer = new CountDownTimer(skb1.getProgress() * 1000 + 100, 1000) {
                @Override
                public void onTick(long l) {
                    updateTimer((int) l / 1000);
                }

                @Override
                public void onFinish() {
                    mediaPlayer.start();
                    resetTimer();
                }
            }.start();
        }
    }

    public void updateTimer(int secondsLeft) {
        int minutes = secondsLeft / 60;
        int seconds = secondsLeft - (minutes * 60);

        String secondString = Integer.toString(seconds);

        if (seconds <= 9) {
            secondString = "0" + secondString;
        }

        tv1.setText(Integer.toString(minutes) + ":" + secondString);
    }

    public void resetTimer() {
        tv1.setText("0:30");
        skb1.setProgress(30);
        skb1.setEnabled(true);
        countDownTimer.cancel();
        btn1.setText("START");
        counterIsActive = false;
    }
}
